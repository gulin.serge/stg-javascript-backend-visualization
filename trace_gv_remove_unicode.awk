#!/usr/bin/awk -f

BEGIN {
  indent_width = 4

  confirmed_unicode_category_spaces_inner = -1
  confirmed_unicode = 0
}

{
  if (calculate_spaces($0) < confirmed_unicode_category_spaces_inner) {
    confirmed_unicode_category_spaces_inner = -1
    confirmed_unicode = 0
  }
}

/NAME.*h\$ghczminternalZCGHCziInternalziUnicodeziCharziUnicodeDataziGeneralCategoryzilvl_1/ {
  confirmed_unicode_category_spaces_inner = calculate_spaces($0) + 1
}

/ARRAYLIT/ {
  if (confirmed_unicode_category_spaces_inner > 0 && calculate_spaces($0) >= confirmed_unicode_category_spaces_inner) {
    confirmed_unicode = 1
  }
}

{
  if (confirmed_unicode == 0) {
    print $0
  }
}

function calculate_spaces(line) {
  match(line, /^ */)
  return int(RLENGTH / indent_width)
}

#!/usr/bin/awk -f

BEGIN {
  previous_line = ""
}

{
  sub(/[\n\r]+/, "+", $0)
  if (calculate_spaces($0) >= 4) {
    print previous_line
    previous_line = $0
    next
  }

  previous_line = previous_line $0
  next
}

END {
  print previous_line
}

function calculate_spaces(line) {
  match(line, /^ */)
  return int(RLENGTH)
}

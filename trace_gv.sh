#!/usr/bin/env bash

# $ ./trace_gv.sh ../ghc-js/_build/stage1/bin/javascript-unknown-ghcjs-ghc

generate_ast_and_tree () {
  local -r WORKING_DIR=$1
  local -r BUNDLE_JS=$2

  local -r AST_JS="${BUNDLE_JS}.tree"

  echo "Create AST ${AST_JS} from Google Closure Compiler over ${BUNDLE_JS}"
	closure-compiler \
    --debug --warning_level QUIET --formatting PRETTY_PRINT \
    --assume_function_wrapper \
    --compilation_level WHITESPACE_ONLY \
    --print_tree \
    "${WORKING_DIR}/HelloJS.jsexe/${BUNDLE_JS}" "${WORKING_DIR}/HelloJS.jsexe/all.externs.js" \
    --js_output_file "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"

  echo "Remove [source_file... section, we do not use it"
  sed -i -r 's/\[source_file.*//' "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"
  echo "Remove [free_call... section, we do not use it"
  sed -i -r 's/\[free_call.*//' "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"
  echo "Remove [added_block... section, we do not use it"
  sed -i -r 's/\[added_block.*//' "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"
  echo "Remove [quoted... section, we do not use it"
  sed -i -r 's/\[quoted.*//' "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"
  echo "Remove [is_parenthesized... section, we do not use it"
  sed -i -r 's/\[is_parenthesized.*//' "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"
  echo "Remove [incrdecr... section, we do not use it"
  sed -i -r 's/\[incrdecr.*//' "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"
  echo "Remove [direct_eval... section, we do not use it"
  sed -i -r 's/\[direct_eval.*//' "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"
  echo "Reformat [length... section, leave only number and cut the beginning space"
  sed -r 's/ \[length: ([0-9]+)\]/\1/' "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}" > "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}.sed" && \
    mv -f "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}.sed" "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"

  echo "String literals may contains newline symbols, we need escape it"
  # TODO: better to support such case in `tree.py` because multline stringliterals could be present and valid
  awk -f ./trace_gv_remove_newlines.awk "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}" > "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}.awk"
  mv -f "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}.awk" "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"

  echo "Remove ROOT header, it is useless for us"
  awk -f ./trace_gv_remove_root.awk "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}" > "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}.awk"
  mv -f "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}.awk" "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"

  echo "Remove unicode list content"
  awk -f ./trace_gv_remove_unicode.awk "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}" > "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}.awk"
  mv -f "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}.awk" "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"

  echo "Remove useless indentation"
  sed -i -r 's/^        //' "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"

  echo "AST   : ${WORKING_DIR}/HelloJS.jsexe/${AST_JS}"
  cp "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}" ./out

  local -r CALL_GV_JS="${BUNDLE_JS}.call.gv"
  local -r MODULES_GV_JS="${BUNDLE_JS}.modules.gv"

  python3 ./tree.py \
    "${WORKING_DIR}/HelloJS.jsexe/${AST_JS}" \
    "${WORKING_DIR}/HelloJS.jsexe/${CALL_GV_JS}" \
    "${WORKING_DIR}/HelloJS.jsexe/${MODULES_GV_JS}" \

  echo "GV  CA: ${WORKING_DIR}/HelloJS.jsexe/${CALL_GV_JS}"
  cp "${WORKING_DIR}/HelloJS.jsexe/${CALL_GV_JS}" ./out
  echo "GV  MO: ${WORKING_DIR}/HelloJS.jsexe/${MODULES_GV_JS}"
  cp "${WORKING_DIR}/HelloJS.jsexe/${MODULES_GV_JS}" ./out
}

main () {
  local -r GHC_COMPILER="${1}"

  local -r WORKING_DIR=$(mktemp -d 2>/dev/null || mktemp -d -t 'mytmpdir')

  {
    echo 'module Main where'
    echo 'main :: IO ()'
    echo 'main = print ("Hello, World!" :: String)'
  } >> "${WORKING_DIR}/HelloJS.hs"

  echo "Build HelloJS:"
  cat "${WORKING_DIR}/HelloJS.hs"

  echo "Use GHC for compile: ${GHC_COMPILER}"
	"${GHC_COMPILER}" -O0 -ddisable-js-minifier -fforce-recomp "${WORKING_DIR}/HelloJS.hs"

  echo "Run at node ${WORKING_DIR}/HelloJS.jsexe/all.js"
	node "${WORKING_DIR}/HelloJS.jsexe/all.js"

  echo "Source: ${WORKING_DIR}/HelloJS.jsexe/all.js"
  cp "${WORKING_DIR}/HelloJS.jsexe/all.js" ./out

  echo "Erase comments by Google Closure Compiler"
  closure-compiler \
    --debug --formatting PRETTY_PRINT --warning_level QUIET \
    --assume_function_wrapper \
    --compilation_level WHITESPACE_ONLY \
    "${WORKING_DIR}/HelloJS.jsexe/all.js" "${WORKING_DIR}/HelloJS.jsexe/all.externs.js" \
    --js_output_file "${WORKING_DIR}/HelloJS.jsexe/all.no_comments.js"

  echo "Source: ${WORKING_DIR}/HelloJS.jsexe/all.no_comments.js"
  cp "${WORKING_DIR}/HelloJS.jsexe/all.no_comments.js" ./out

  generate_ast_and_tree \
    "${WORKING_DIR}" \
    "all.no_comments.js"

  echo "Prevent inlining for easier removal unicode array"
  sed -i -r "s#function h\$rawStringData#/** @noinline */ function h\$rawStringData#" "${WORKING_DIR}/HelloJS.jsexe/all.js"

  echo "Minimize first pass with Google Closure Compiler"
  closure-compiler \
    --debug --formatting PRETTY_PRINT --warning_level QUIET \
    --assume_function_wrapper \
    --compilation_level ADVANCED_OPTIMIZATIONS \
    "${WORKING_DIR}/HelloJS.jsexe/all.js" "${WORKING_DIR}/HelloJS.jsexe/all.externs.js" \
    --js_output_file "${WORKING_DIR}/HelloJS.jsexe/all.min.js"

  echo "Run at node ${WORKING_DIR}/HelloJS.jsexe/all.min.js"
  node "${WORKING_DIR}/HelloJS.jsexe/all.min.js"

  echo "Source: ${WORKING_DIR}/HelloJS.jsexe/all.min.js"
  cp "${WORKING_DIR}/HelloJS.jsexe/all.min.js" ./out

  generate_ast_and_tree \
    "${WORKING_DIR}" \
    "all.min.js"
}

main "$@"
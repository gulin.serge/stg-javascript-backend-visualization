let
  pinned = import (import ./nix/pinned-nixpkgs.nix) {
    config = { allowUnfree = true; enableParallelBuilding = true; allowBroken = true; };
  };
in { pkgs ? pinned }:
  with pkgs;
  let
    kernelPython = python311.override {
      packageOverrides = self: super: {
        ipykernel = super.ipykernel.overridePythonAttrs(old: {
          doCheck = false;
        });
        dill = super.dill.overridePythonAttrs(old: rec {
          version = "0.3.6";
          src =  super.fetchPypi {
            pname = "dill";
            inherit version;
            sha256 = "5dtV82h4Vtj72rAC7XhUThxFWaEwMCaT2Dnf6Pk/I3M=";
          };
        });
        PyFunctional = python311Packages.buildPythonPackage rec {
          pname = "PyFunctional";
          version = "1.4.3";

          src = python311Packages.fetchPypi {
            inherit pname version;
            sha256 = "EcMT/iUbJpxlBmiRNUVqBbxab6EpydArRF84PU9BHhA=";
          };

          checkInputs = [ self.pytest ];
          propagatedBuildInputs = [
              self.future
              self.six
              self.dill
              self.bz2file
              self.tabulate
            ];

          doCheck = false;
        };
      };
    };

    kernelEnv = kernelPython.withPackages (p: with p; [
      ipykernel
      ipympl

      PyFunctional
      regex

      networkx

      pygraphviz
      pydot
      lxml

      anytree
    ]);

  in mkShell {
    packages = [
      gnumake
      gnused
      gawk
      closurecompiler
      kernelEnv
      nodejs_20
    ];
    shellHook = ''
      export PYTHONDONTWRITEBYTECODE=1

      unset SOURCE_DATE_EPOCH
    '';
  }
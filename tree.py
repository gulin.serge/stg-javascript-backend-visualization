#!/usr/bin/env python3

# It discovers the full call graph.
# No need to pass any points: it calculates full graph from script tree.
# ./callgraph.tree.py "./out/all.no_comments.js.tree" "./graph.call.gv" "./graph.modules.gv"

import math
import sys
import anytree
import anytree.exporter
import re
from typing import Literal

LEVEL_DELIM = 4
STG_DESCRIPTION_FNS = [
  "h$o",
  "$h$o$$",
  "h$sti",
  "$h$sti$$",
  "h$stc",
  "$h$stc$$",
  "h$stl",
  "$h$stc$$",
  "h$d",
  "$h$d$$"

]
STG_TRANSITIVE_FNS = list(map(re.compile, [
  "^\$*h\$d[0-9]+\$*$",
  "^\$*h\$p[0-9]+\$*$",
  "^\$*h\$pp[0-9]+\$*$",
  "^\$*h\$c[0-9]+\$*$",
  "^\$*h\$e\$*$"
]))
JS_TARGETS_PASSTHOUGH = [
  'IF',
  'SUB',
  'TYPEOF',
  'NOT',
  'NEW',
  'EXPR_RESULT',
  'ADD',
  'HOOK',
  'THROW',
  'INSTANCEOF',
  'BLOCK',
  'ARRAYLIT',
  'MUL',
  'DIV',
  'RSH',
  'URSH',
  'OBJECTLIT',
  'BITNOT',
  'STRING_KEY',
  'SWITCH',
  'CASE',
  'RETURN',
  'GETELEM',
  'DEFAULT_CASE',
  'ASSIGN',
  'INC',
  'DEC',
  'ASSIGN_ADD',
  'ASSIGN_SUB',
  'BITAND',
  'BITOR',
  'GT',
  'GE',
  'LT',
  'LE',
  'NE',
  'AND',
  'OR',
  'SHEQ',
  'SHNE',
  'EQ',
  'LSH',
  'FOR',
  'FOR_IN',
  'DO',
  'WHILE',
  'DELPROP',
  'TRY',
  'CATCH',
  'MOD',
  'REGEXP',
  'COMMA',
  'ASSIGN_URSH',
  'ASSIGN_RSH',
  'ASSIGN_BITOR',
  'ASSIGN_BITAND',
  'ASSIGN_DIV',
  'ASSIGN_OR',
  'EXPONENT',
  'BITXOR',
  'POS',
  'DEFAULT_VALUE',
  'TEMPLATELIT',
  'TEMPLATELIT_SUB',
  'OPTCHAIN_CALL',
  'IN',
  'LABEL'
]

JS_TARGETS_SKIP = [
  'STRINGLIT',
  'NEG',
  'NUMBER',
  'NULL',
  'TRUE',
  'FALSE',
  'BREAK',
  'EMPTY',
  'VOID',
  'LABEL_NAME',
  'CONTINUE',
  'TEMPLATELIT_STRING',
  # TODO:
  'MEMBER_FUNCTION_DEF'
]

JS_VARS_DECLARATIONS = [
  'VAR',
  'CONST',
  'LET'
]

JS_TARGETS_PROCESS = JS_VARS_DECLARATIONS + [
  'CALL',
  'FUNCTION',
  'GETPROP',
  'THIS',
  'NAME',
  'PARAM_LIST'
]

def parse_line_elements(elements):
  type = elements[0]

  if type == 'EMPTY':
    return {
      "type": type,
      "name": None,
      "pos": elements[-1],
      "size": 0
    }

  size = int(elements[-1])
  name = ' '.join(elements[1:-2])

  parsed_elements = {
    "type": type,
    "name": None if name == '' else name,
    "pos": elements[-2],
    "size": size
  }

  return parsed_elements

def parse_line(l: str):
  content = l.lstrip()
  if (content == ''):
    return None

  parsed = parse_line_elements(list(filter(lambda x: x != '', content.strip().split(' '))))

  current_level = math.ceil((len(l) - len(content)) / LEVEL_DELIM) + 1

  return current_level, parsed

HASKELL_NAME_PREFIXES_ODERED = [
  # Minified version from google closure
  '$h$$',
  # Minified version from google closure
  '$h$',
  # See compiler/GHC/StgToJS/Symbols.hs
  'h$$',
  # See compiler/GHC/StgToJS/Symbols.hs
  'h$'
]

def is_node_name_haskell_generated(name):
  if name == None: return False
  return name.startswith(HASKELL_NAME_PREFIXES_ODERED[0]) \
    or n.name.startswith(HASKELL_NAME_PREFIXES_ODERED[2])

def is_node_name_haskell_defined(name):
  if name == None: return False
  if is_node_name_haskell_generated(name): return False
  return name.startswith(HASKELL_NAME_PREFIXES_ODERED[1]) \
    or name.startswith(HASKELL_NAME_PREFIXES_ODERED[3])

def is_node_name_haskell(name):
  return is_node_name_haskell_generated(name) or is_node_name_haskell_defined(name)

HASKELL_JAVASCRIPT_GLOBAL_SCOPE_MODULE_NAME = "<haskell_builtin_javascript>"

def rewrite_name_from_zencoding(name):
  if not is_node_name_haskell(name): return None

  # See libraries/ghc-boot/GHC/Utils/Encoding.hs
  # See compiler/GHC/Types/Name/Occurrence.hs
  node_name_begin_normalized = name \
    .replace(HASKELL_NAME_PREFIXES_ODERED[0], "", 1) \
    .replace(HASKELL_NAME_PREFIXES_ODERED[1], "", 1) \
    .replace(HASKELL_NAME_PREFIXES_ODERED[2], "", 1) \
    .replace(HASKELL_NAME_PREFIXES_ODERED[3], "", 1) \
    .replace('ZZ', "Z") \
    .replace('ZC', ":") \
    .replace('zz', "z") \
    .replace('zm', "-") \
    .replace('zi', ".") \
    .replace('zu', "_") \
    .replace('zd', "$") \
    .replace('zh', "#")

  node_haskell_module = HASKELL_JAVASCRIPT_GLOBAL_SCOPE_MODULE_NAME
  node_haskell_name = name
  node_name_splitted_by_module = node_name_begin_normalized.split(':')
  if len(node_name_splitted_by_module) == 2:
    node_haskell_module = node_name_splitted_by_module[0]
    node_haskell_name = node_name_splitted_by_module[1]
  elif len(node_name_splitted_by_module) > 2:
    node_haskell_module = node_name_splitted_by_module[0]
    node_haskell_name = ':'.join(node_name_splitted_by_module[1:])

  return node_haskell_module, node_haskell_name

def stack_to_call_tree(parsed_line, node):
  def traverse_to_parent_ident(n, ident=0):
    while n.ident >= ident:
      n = n.parent
    return n

  current_stack_ident, current_stack_content = parsed_line

  if node.ident >= current_stack_ident:
    node = traverse_to_parent_ident(node, current_stack_ident)

  if current_stack_content['type'] in JS_TARGETS_SKIP:
    return node

  if current_stack_content['type'] in JS_TARGETS_PROCESS \
    or current_stack_content['type'] in JS_TARGETS_PASSTHOUGH:
    child_node = anytree.AnyNode(
      name=current_stack_content['name'],
      type=current_stack_content['type'],
      pos=current_stack_content['pos'],
      volume=current_stack_content['size'],
      ident=current_stack_ident,
      stg=None,
      parent=node,
      locals=set() if current_stack_content['type'] == 'FUNCTION' else None,
      params=set() if current_stack_content['type'] == 'FUNCTION' else None,
      haskell_module=None
    )
    return child_node

  raise NameError('Unexpected term type:', current_stack_content['type'], current_stack_content['pos'])

# If var is global it will not register it.
def try_register_var(function_node, name: str, type: Literal['local'] | Literal['params']):
  while function_node.type != 'FUNCTION' and not function_node.is_root:
    function_node = function_node.parent

  # if var is global
  if function_node.is_root:
    return

  if type == 'local':
    function_node.locals.add(name)
  elif type == 'params':
    function_node.params.add(name)
  return

def call_tree_compact(node):
  current_children = node.children

  if node.type in (['CALL'] + JS_VARS_DECLARATIONS) and len(current_children) > 0:
    if current_children[0].name in STG_DESCRIPTION_FNS:
      node.stg = current_children[0].name
      current_children = list(current_children[0].siblings)
    if len(current_children) > 0:
      node.name = current_children[0].name
      current_children = current_children[1:]

  if node.type in JS_VARS_DECLARATIONS:
    try_register_var(node, node.name, 'local')

  if node.parent != None and node.parent.type in JS_VARS_DECLARATIONS:
    try_register_var(node, node.name, 'local')

  if node.parent != None and node.parent.type == 'GETPROP' and node.type == 'THIS':
    node.name = 'this'
    return node

  if node.type == 'PARAM_LIST':
    param_list_children = []
    if len(node.children) > 0:
      for p in node.children:
        if p.type == 'NAME':
          try_register_var(node, p.name, 'params')
        elif p.type == 'DEFAULT_VALUE' and len(p.children) == 1:
          p_n = p.children[0]
          try_register_var(node, p_n.name, 'params')
        elif p.type == 'DEFAULT_VALUE' and len(p.children) == 2:
          p_n = p.children[0]
          try_register_var(node, p_n.name, 'params')
          p_v = call_tree_compact(p.children[1])
          if p_v != None:
            param_list_children.append(p_v)
        else:
          raise ValueError('Unexpected PARAM_LIST')
    if len(param_list_children) > 0:
      node.children = param_list_children
      return node
    else:
      return None

  if len(current_children) == 0 and (node.name == None):
    return None

  if node.type == 'GETPROP' and node.name != None and len(current_children) == 1:
    child_node = call_tree_compact(current_children[0])
    if child_node != None:
      if child_node.name != None:
        child_node.name = child_node.name + "." + node.name
      elif len(child_node.children) == 2:
        ch_k = child_node.children[0]
        ch_v = child_node.children[1]
        child_node.name = ch_k.name + "[" + ch_v.name + "]" + "." + node.name
      elif child_node.type == 'NEW':
        # TODO: unwrap new
        child_node.name = "new (...)"
      else:
        print('node', anytree.RenderTree(node))
        print('child_node', anytree.RenderTree(child_node))
        raise ValueError('Unexpected child_node')
      return child_node
    else:
      return node

  if len(current_children) == 1 and node.name == None:
    return call_tree_compact(current_children[0])

  updated_children = []
  uniq_names = set()

  # Returns True if something COULD BE added
  def try_add_unique_children(n):
    if n == None:
      # If nothing to add let's treat it as "empty add"
      return True

    if len(n.children) == 0 and n.name != None:
      if not (n.name in uniq_names):
        updated_children.append(n)
        uniq_names.add(n.name)
      return True

    return False

  for c in current_children:
    processed = call_tree_compact(c)

    # Remove repeated items
    if try_add_unique_children(processed):
      continue
    # Distile all terms which content could have targets/terms to process further
    # Distile unnamed functions
    # Distile all local vars
    elif processed.type in JS_TARGETS_PASSTHOUGH \
      or (processed.type == 'FUNCTION' and processed.name == None) \
      or (processed.type in JS_VARS_DECLARATIONS and processed.parent.name != BUNDLE_NAME):
      for cc in processed.children:
        processed_cc = call_tree_compact(cc)
        (not try_add_unique_children(processed_cc)) \
          and updated_children.append(processed_cc)
    # Distile all stg internal stuff
    elif processed.type != 'FUNCTION' and processed.name != None and any(regex.match(processed.name) for regex in STG_TRANSITIVE_FNS):
      updated_children.append(anytree.AnyNode(
        name=processed.name,
        type=processed.type,
        pos=processed.pos,
        volume=processed.volume,
        stg=processed.stg,
        ident=processed.ident,
        locals=processed.locals,
        params=processed.params,
        haskell_module=processed.haskell_module
      ))
      for cc in processed.children:
        processed_cc = call_tree_compact(cc)
        (not try_add_unique_children(processed_cc)) \
          and updated_children.append(processed_cc)
    else:
      updated_children.append(processed)

  node.children = updated_children

  return node

# Empty children at top level means no dependencies
def remove_top_level_empty_children(top_node):
  new_children = []
  for c in top_node.children:
    if len(c.children) == 0:
      continue
    new_children.append(c)

  top_node.children = new_children
  return top_node

# If node is a function but its child is local or param -- drop it
# We do not to track them because we need only actual calls with actual arguments
def remove_function_level_params_and_locals(top_node, known_params_locals = set()):
  if len(top_node.children) == 0: return top_node

  if top_node.type == 'FUNCTION':
    params_locals = known_params_locals | top_node.params | top_node.locals
    filtered_children = []
    for c in top_node.children:
      if len(c.children) == 0:
        if c.name != None and c.name.replace('[', '.').split('.')[0] in params_locals:
          continue
        else:
          filtered_children.append(c)
      else:
        filtered_children.append(remove_function_level_params_and_locals(c, params_locals))
    top_node.children = filtered_children
    return top_node
  else:
    filtered_children = []
    for c in top_node.children:
      if len(c.children) == 0:
        filtered_children.append(c)
      else:
        filtered_children.append(remove_function_level_params_and_locals(c, known_params_locals))
    top_node.children = filtered_children
    return top_node

BUNDLE_NAME = 'SCRIPT_BUNDLE_ROOT'

tree_functions_sizes = {}
tree_others_sizes = {}

def node_size_attrs_by_name(name):
  size = 0

  if name == BUNDLE_NAME:
    return BUNDLE_NAME, size

  is_function = False
  if name in tree_functions_sizes:
    size += tree_functions_sizes[name]
    is_function = True
  if name in tree_others_sizes:
    size += tree_others_sizes[name]

  haskell_source = None
  if is_node_name_haskell_generated(name):
    haskell_source = 'generated'
  elif is_node_name_haskell_defined(name):
    haskell_source = 'defined'

  if haskell_source == None:
    if is_function:
      return "javascript_function", size
    return "javascript_variable", size

  if haskell_source == 'generated':
    if is_function:
      return "haskell_function_generated", size
    return "haskell_variable_generated", size

  if haskell_source == 'defined':
    if is_function:
      return "haskell_function_defined", size
    return "haskell_variable_defined", size

  raise ValueError('Unexected state:', is_function, haskell_source)

def nodeattrfunc_call(node):
  domain, size = node_size_attrs_by_name(node.name)
  return "domain=%s size=%i" % (domain, size)

def edgeattrfunc_call(n_from, n_to):
  if n_from.stg != None:
    return "relation=%s" % n_from.stg
  if n_from.name in tree_functions_sizes:
    return "relation=call"

  return "relation=unknown"

def nodeattrfunc_modules(node):
  if node.origin_name != None:
    return "label=%s domain=%s size=%i origin_name=%s" % (node.name, node.domain, node.volume, node.origin_name)

  return "label=%s size=%i" % (node.name, node.volume)

def find_children_by_name(node, name):
  for c in node.children:
    if c.name == name:
      return c

  return None

def build_haskell_modules_tree(bundle_root, haskell_module, origin_name):
  current_node = bundle_root

  haskell_module_package, haskell_module_name = haskell_module

  haskell_module_package_existing = find_children_by_name(current_node, haskell_module_package)
  if haskell_module_package_existing == None:
    haskell_module_package_existing = anytree.AnyNode(
      name=haskell_module_package,
      volume=0,
      parent=current_node,
      domain=None,
      origin_name=None
    )

  current_node = haskell_module_package_existing

  # TODO: x != ''
  # Why it can be empty?
  splitted_name = list(filter(lambda x: x != '', haskell_module_name.replace('_', '._', 1).strip().split('.')))

  for i in range(0, len(splitted_name)):
    partial_name = '.'.join(splitted_name[0:i+1])

    p_node = find_children_by_name(current_node, partial_name)

    if p_node == None:
      p_node = anytree.AnyNode(
        name=partial_name,
        volume=0,
        parent=current_node,
        origin_name=None,
        domain=None
      )

    current_node = p_node

  domain, size = node_size_attrs_by_name(origin_name)

  p_node.origin_name = origin_name
  p_node.volume = size
  p_node.domain = domain

def calc_volume_sum_for_haskell_modules_tree(root_node):
  total_volume = root_node.volume

  for c in root_node.children:
    total_volume += calc_volume_sum_for_haskell_modules_tree(c)

  root_node.volume = total_volume
  return total_volume

with open(sys.argv[1], 'r') as f:
  lines = f.read().splitlines()
  current_node = anytree.AnyNode(ident=-1, type=None, pos=None, stg=None, volume=None, name=BUNDLE_NAME, locals=None, params=None, haskell_module=None)
  for l in lines:
    parsed = parse_line(l)
    current_node = stack_to_call_tree(parsed, current_node)

  while not current_node.is_root:
    current_node = current_node.parent

  current_node = call_tree_compact(current_node)
  current_node = remove_function_level_params_and_locals(current_node)

  for n in anytree.PreOrderIter(current_node):
    n.haskell_module = rewrite_name_from_zencoding(n.name)

    if n.type == 'FUNCTION':
      if not n in tree_functions_sizes:
        tree_functions_sizes[n.name] = n.volume
      else:
        tree_functions_sizes[n.name] += n.volume
    elif n.type != 'FUNCTION':
      if not n in tree_others_sizes:
        tree_others_sizes[n.name] = n.volume
      else:
        tree_others_sizes[n.name] += n.volume
  # print(tree_functions_sizes)
  # print(tree_others_sizes)

  bundle_analyzer_node = anytree.AnyNode(name=BUNDLE_NAME, volume=0, domain=None, origin_name=None)
  anytree.AnyNode(name="<javascript_global>", parent=bundle_analyzer_node, volume=0, domain=None, origin_name=None)
  for n in anytree.PreOrderIter(current_node):
    if n.haskell_module != None:
      build_haskell_modules_tree(bundle_analyzer_node, n.haskell_module, n.name)
    elif n.name != None and n.name != BUNDLE_NAME:
      build_haskell_modules_tree(bundle_analyzer_node, ("<javascript_global>", n.name), n.name)
  calc_volume_sum_for_haskell_modules_tree(bundle_analyzer_node)
  # print(anytree.RenderTree(bundle_analyzer_node))

  with open(sys.argv[3], 'w') as f:
    f.writelines(anytree.exporter.UniqueDotExporter(bundle_analyzer_node, nodeattrfunc=nodeattrfunc_modules))

  current_node_no_empty_top = remove_top_level_empty_children(current_node)
  # print(anytree.RenderTree(current_node_no_empty_top))

  with open(sys.argv[2], 'w') as f:
    f.writelines(anytree.exporter.DotExporter(current_node_no_empty_top, nodeattrfunc=nodeattrfunc_call, edgeattrfunc=edgeattrfunc_call))
